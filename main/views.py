from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse
from .models import Pesan, Tempat
import requests
import random
from django.forms.models import model_to_dict
from datetime import datetime
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import PostSerializer
from main import serializers
from .forms import inputForm, jalanForm


def landing(request):
    return render(request, 'main/landing.html')

@api_view(['GET'])
def postList(request):
    pesan = Pesan.objects.all().order_by('-id')
    serializer = PostSerializer(pesan, many=True)
    return Response(serializer.data)

    # if (form.is_valid() and request.method == "POST"):
    #     form.save()
    #     return  HttpResponseRedirect('/message_list')
@api_view(['POST'])
def createPost(request):
    serializer = PostSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()

        return Response(serializer.data)

def post(request):
    form = inputForm(initial={'sender_name':'anonymous', 'receiver_name':'everyone'})
    return render(request, 'main/anonymous_post.html', {'form' : form})


###! message feature ####
def add_message(request):
    form = inputForm()
    form.fields['sender_name'].initial = 'anonymous'
    form.fields['receiver_name'].initial = 'someone better than me'
    return render(request, 'main/add.html', {'form' : form})

def message_list(request):
    pesan_isi = Pesan.objects.all()
    context = {
        'pesan' : pesan_isi
    }
    return render(request, 'main/list.html', context)

def form(request):
    form = inputForm(request.POST)
    context = {
        'form' : form
    }
    if(form.is_valid() and request.method == "POST"):
        form.save()
        return  HttpResponseRedirect('/message_list')

    return render(request, 'main/add.html', context)

def delete(request, id):
    try:
        Pesan.objects.get(id=id).delete()
        return HttpResponseRedirect('/message_list')
    except Exception:
        return HttpResponseRedirect('/message_list')

###! Bitcoin Price Feature ###
def price_today(request):
    response = requests.get('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd&include_24hr_change=true').json()
    
    data = {}
    data["price"]  = response["bitcoin"]["usd"]
    data["change"] = round(response["bitcoin"]["usd_24h_change"], 2)
    if data["change"] > 0:
        data["up"] = "true"

    time = datetime.now()
    time_formatted = time.strftime("%H") +":" + time.strftime("%M") + " " + time.strftime("%x")

    context = {'data':data, 'time':time_formatted}
    return render(request, 'main/price_today.html', context)

###! Place Search & Randomizer ###
def place_randomizer(request):

    form = jalanForm(request.GET)
    tempat = Tempat.objects.all()
    tempat_list = []
    for i in tempat:
        tempat_dict = model_to_dict(i)
        tempat_list.append(tempat_dict)

    context = {'form':jalanForm(), 'tempat_list':tempat_list}

    if request.GET.get('submit') == 'clicked':
        location = request.GET.get('location').upper()
        activity = request.GET.get('activity').upper()
        price = request.GET.get('price').upper()
        tempat = Tempat.objects.filter(lokasi=location).filter(tipe_tempat=activity).filter(mampu_beli=price)
        tempat_list = []
        for i in tempat:
            tempat_dict = model_to_dict(i)
            tempat_list.append(tempat_dict)

        try:
            output = random.choice(tempat_list)
            context = {'form':form, 'output':output, 'tempat_list':tempat_list, 'location': location}

            return render(request, 'main/jalan_kemana.html', context)
        
        except:
            output = 'ngg ada bos'
            context = {'form':form, 'output':output}
            return render(request, 'main/jalan_kemana.html', context)
    
    else:
        return render(request, 'main/jalan_kemana.html', context)


def nggabut(request):
    return render(request, 'main/nggabut.html')