from rest_framework import serializers
from .models import Pesan

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pesan
        fields = '__all__'