from django import forms
from django.forms import fields
from .models import Pesan, Tempat

class inputForm(forms.ModelForm):
    class Meta:
        model  = Pesan
        fields = '__all__'
    
class jalanForm(forms.Form):
    location = forms.ChoiceField(choices=[('bintaro','Bintaro'),
        ('bsd','BSD')], widget=forms.RadioSelect(attrs={'class': "form-check-inline"}), label='Mau Jalan Kemana Bestie?')
    activity = forms.ChoiceField(choices=[('makan','Makan'),
        ('nongkrong','Nongkrong')], widget=forms.RadioSelect(attrs={'class': "form-check-inline"}), label='Mau Ngapain Nih Bestie?')
    price = forms.ChoiceField(choices=[('murah','Murah'),
    ('sedeng','Sedeng'), ('mehong','Mehong')], widget=forms.RadioSelect(attrs={'class': "form-check-inline"}), label='Mau Harga Berapa Bestie?')
    class Meta:
        model  = Tempat
