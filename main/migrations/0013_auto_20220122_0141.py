# Generated by Django 3.2.11 on 2022-01-21 18:41

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0012_pesan_datetime'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pesan',
            name='datetime',
        ),
        migrations.AddField(
            model_name='pesan',
            name='date',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
