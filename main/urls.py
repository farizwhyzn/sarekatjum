from django.urls import path
from . import views
# from .api import TempatViewSet
# from rest_framework import routers

app_name = 'main'

# router = routers.DefaultRouter()
# router.register('api/main', TempatViewSet, 'viewset')

urlpatterns = [
    path('', views.landing, name='landing'),
    path('add_message/', views.form, name='add_message'),
    path('message_list/', views.message_list, name='message_list'),
    path('price_today/', views.price_today, name='price_today'),
    path('randomizer/', views.place_randomizer, name='randomizer'),
    path('nggabut/', views.nggabut, name='nggabut'),
    path('api/viewall/', views.postList),
    path('api/create/', views.createPost),
    path('post/', views.post, name='post')
]
