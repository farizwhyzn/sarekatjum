from django.db import models

class Pesan(models.Model):
    sender_name   = models.CharField(max_length = 50)
    receiver_name = models.CharField(max_length = 50)
    message       = models.TextField()
    date          = models.DateField(auto_now_add=True)

class Tempat(models.Model):
    nama_tempat	= models.CharField(max_length = 255)
    tipe_tempat	= models.CharField(max_length = 255)
    lokasi = models.CharField(max_length = 255)
    keterangan_tempat = models.TextField()
    jam_buka = models.CharField(max_length = 255)
    price_range_spesifik = models.CharField(max_length = 255, default=None, blank=True, null=True)	
    mampu_beli = models.CharField(max_length = 255)
    link_maps = models.CharField(max_length = 255)
    parkir = models.CharField(max_length = 255, default=None, blank=True, null=True)
    plus = models.TextField(default=None, blank=True, null=True)
    minus = models.TextField(default=None, blank=True, null=True)
    class Meta:
        db_table = "tempat"